package br.com.geradordedevs.vehicleregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleRegistrationProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleRegistrationProjectApplication.class, args);
	}

}
